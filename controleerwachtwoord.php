<!DOCTYPE html>
<?php
session_start();
$inlognaam = trim($_POST['LogonName']);
$wachtwoord = trim($_POST['wachtwoord']);
$sleutel = hash('sha256', $wachtwoord);

$user = "root";
$pass = "";
$dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);

$sth = $dbh->prepare("SELECT * FROM klant WHERE LogonName= '$inlognaam' AND HashedPassword= '$sleutel' AND IsPermittedToLogon= 1");

$sth->bindValue(':LogonName', $inlognaam, PDO::PARAM_STR);
$sth->bindValue(':wachtwoord', $wachtwoord, PDO::PARAM_STR);

$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);

$ingelogd = false;
if (count($result) > 0) {
    $ingelogd = true;

    $_SESSION['LogonName'] = $inlognaam;
    $_SESSION['status'] = 1;
}
?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <?php
        if ($ingelogd) {
            header("Location: index.php");
        } else {
            header("Location: inlogscherm.php");
        }
        //
        ?>
    </body>
</html>
