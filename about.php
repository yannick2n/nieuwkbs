<!DOCTYPE html>
<?php
include 'db.php';
?>
<?php
//Start session
session_start();
// The connection to the database
include 'connect.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<head>
	<title>Over Ons</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
        <!--===============================================================================================-->
    </head>
    <div class="header1">
        <!-- Header desktop -->
        <div class="wrap_header">
            <!-- Logo -->
            <a href="index.php" class="logo">
                <img src="images/wwi.png" alt="IMG-LOGO">
            </a>

            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <?php
                                    while ($row = $hresult->fetch_assoc()) {
                                        echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="about.php">Over ons</a>
                        </li>

                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <!-- Header Icon -->
            <div class="header-icons">

                <div class="header-wrapicon2">
                    <li class="fa fa-search fa-2x header-icon1 js-show-header-dropdown"

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <strong>Search:</strong>
                        <div class="header-cart-total">
                        </div>
                        <form method="get" action="zoeken.php">
                            <input class="zoekbalk" type="text" placeholder="Product" name="zoeken" >
                            <input class="zoekknop" type="submit" value="Zoeken" name="submit">
                        </form>
                    </div>
                </div>

                <span class="linedivide1"></span>

                <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                    <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                    <span class="header-icons-noti"><?php echo $_SESSION['waardes'];?></span>
                </div>
            </div>
        </div>
    </div>
<body class="animsition">




	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
                                            <img src="images/over_ons.jpg" alt="IMG-ABOUT">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Over ons
					</h3>

					<p class="p-b-28">
						Wij zijn de ICT heroes van groep 3 en doen het met plezier, JIPPIE!
					</p>
				</div>
			</div>
		</div>
	</section>


	<!-- Footer -->
   
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90 row">
			<div class="w-size7 p-t-30 p-l-250 p-r-20 respon4 col-sm-4">
                            
				<h4 class="s-text12 p-b-30">
					Categorieën
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Novelty items
						</a>
					</li>
                                        
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Clothing
						</a>
					</li>
                                        
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Mugs
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							T-shirts
						</a>
					</li>
                                        
                                        <li class="p-b-9">
						<a href="#" class="s-text7">
							Airline Novelties
						</a>
					</li>
				</ul>
			</div>
			<div class="flex-w p-b-90 rijcatagorie">
                                <ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Computing Novelties
                                                </a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							USB Novelties
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Furry footwear
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Toys
						</a>
					</li>
                                        
                                        <li class="p-b-9">
						<a href="#" class="s-text7">
							Packaging materials
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
				<h4 class="s-text12 p-b-30">
					Help
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Order traceren
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Retourneren
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Versturen
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							FAQs
						</a>
					</li>
				</ul>
			</div>

			
		</div>
			 <div class="t-center s-text8 p-t-20">
            Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
        </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection -->
	<div id="dropDownSelect1"></div>
	<div id="dropDownSelect2"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});

		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect2')
		});
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
