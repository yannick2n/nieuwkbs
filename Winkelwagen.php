<?php
include 'header.php';
include 'db.php';
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>
            WideWorldImporters
        </title>
        <link rel="stylesheet" type="text/css" href="css/styleproduct.css">
        <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    </head>
    <body>
    <center>
        <div class="shoppingcart">
            <table BORDER="1" FRAME="hsides" RULES="groups" >
                <tr class="lijn">
                    <th class="itemname">Artikel</th>
                    <th class="hoeveelheid">&nbsp;&nbsp;Hoeveelheid&nbsp;&nbsp;</th>
                    <th class="prijs">&nbsp;&nbsp;Prijs p.s.&nbsp;&nbsp;</th>
                    <th class="totaal">&nbsp;&nbsp;Totaal Product&nbsp;&nbsp;</th>
                    <th class="action">&nbsp;&nbsp;Action&nbsp;&nbsp;</th>
                </tr>
                <?php
                $sub=0;
                $waardes = 0;
                $numbers = 0;
                foreach ($_SESSION as $index => $waarde) {
                    if (strpos($index, 'id') === 0) {
                        $id = str_replace('id', '', $index);
                        $sql = "SELECT StockitemName, recommendedretailprice
                            FROM StockItems s 
                            JOIN StockItemStockGroups g 
                            ON s.stockitemid = g.stockitemid 
                            JOIN StockGroups sg 
                            ON g.stockgroupid = sg.StockGroupID
                            WHERE s.StockitemID = " . $id;
                        $result = $conn->query($sql);
                        $numbers = $result->num_rows;
                        if ($result->num_rows > 0) {
                            $category = array();
                            while ($row = $result->fetch_assoc()) {
                                $name = $row["StockitemName"];
                                $price = $row["recommendedretailprice"];
                            }
                            echo "<tr><td class='itemnameproduct'>$name &nbsp;&nbsp;</td> <td class='waarde'> <a href='verwerk.php?id=$id&action=remove'><strong>-</strong></a> $waarde ";
                            ?>
                            <a href="verwerk.php?id=<?php echo $id; ?>&action=add"><strong>+</strong></a></td>
                            <?php
                            echo "<td class='prijsproduct'>€$price</td> <td class='prijsproduct'>€" . $waarde1=$price * $waarde . "</td>";
                            ?>
                            <td><a class="removebutton" href="verwerk.php?id=<?php echo $id; ?>&action=removeall">&nbsp;Remove</a></td></tr>
                            <?php
                            $sub += (double)$waarde1;
                            $waardes += (double)$waarde;

                        } else {
                            echo "Producten";
                        }
                    }
                }

                $_SESSION['waardes'] = $waardes;
                $totaal = 0;
                $btw = $sub * 0.21;
                if ($numbers > 0 && $sub <= 150 ) {
                    $verzend = 6.95;
                    $tekst = FALSE;
                } else {
                    $verzend = 0;
                    $tekst = TRUE;
                }


                $totaalprijs = $sub + $verzend;
                $conn->close();
                ?>
            </table>
            <table class="prijzen" BORDER="1" FRAME="hsides">
                <tr>
                    <th>Subtotaal</th>
                    <th class="sub">€<?php echo $sub; ?></th>
                </tr>
                <tr>
                    <th>BTW</th>
                    <th class="btw">€<?php echo round($btw, 2); ?></th>
                </tr>
                <tr>
                    <th>Verzendkosten</th>
                    <th class="verzend">€<?php echo $verzend; ?></th>
                </tr>
                <tr>
                    <th>Totaal</th>
                    <th class="totaalprijs">€<?php echo $totaalprijs; ?></th>
                </tr>
            </table>
            <div class="">
                <p><?php
                    $kosten = 150 - $sub;
                    $afronding = round($kosten, 2);
                    if ($tekst == TRUE) {
                    print("Er worden geen verzendkosten in rekening gebracht");
                    } elseif ($tekst == FALSE) {
                    print("Je moet nog voor $afronding bestellen voor gratis verzendkosten");
                    }


                    ?></p>

            </div>
        </div>
        <div class="row winkelwagenbuttons">
            <div class="verder">
        <a class="btnAddAction" href="categorie.php?id=1"><p><button>&nbsp;&nbsp;Verder winkelen&nbsp;&nbsp;</button></p></a>
            </div>
            <div class="afrekenen">
        <a class="btnAddAction" href="afrekenen.php"><p><button>&nbsp;&nbsp;Afrekenen&nbsp;&nbsp;</button></p></a>
            </div>
        </div>
    </center>
    <!-- Footer -->
    <footer class="bg6 p-t-45 p-b-43 p-l-65 p-r-65a p-lr-0-xl1">
        <div class="flex-w p-b-90">
            <div class="w-size6 p-t-30 p-l-15 p-r-15 respon6">
                <h4 class="s-text12 p-b-30">
                    Categorieën
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Novelty items
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Clothing
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Mugs
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            T-shirts
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Airline Novelties
                        </a>
                    </li>
                </ul>
            </div>
            <div class="flex-w p-b-90 rijcatagorie">
                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Computing Novelties
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            USB Novelties
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Furry footwear
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Toys
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Packaging materials
                        </a>
                    </li>
                </ul>
            </div>

            <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
                <h4 class="s-text12 p-b-30">
                    Help
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Order traceren
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Retourneren
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Versturen
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            FAQs
                        </a>
                    </li>
                </ul>
            </div>


        </div>
        <div class="t-center s-text8 p-t-20">
            Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
        </div>
    </footer>
    </div>


    <!-- Back to top -->
    <div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
    </div>

    <!-- Container Selection1 -->
    <div id="dropDownSelect1"></div>



    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/slick/slick.min.js"></script>
    <script type="text/javascript" src="js/slick-custom.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>

    </body>
</html>
