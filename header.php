<?php
//Start session
session_start();
// The connection to the database
include 'connect.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s
            JOIN StockItemStockGroups g
            ON s.stockitemid = g.stockitemid
            JOIN StockGroups sg
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<head>
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/styleproduct.css">
    <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    <!--===============================================================================================-->
</head>
<div class="header1">
    <!-- Header desktop -->
    <div class="wrap_headerwinkelwagen">
        <!-- Logo -->
        <a href="index.php" class="logo">
            <img src="images/wwi.png" alt="IMG-LOGO">
        </a>

        <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link dropbtn">Producten</a>
                            <div class="dropdown-content">
                                <?php
                                while ($row = $hresult->fetch_assoc()) {
                                    echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                }
                                ?>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="about.php">Over ons</a>
                    </li>

                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Header Icon -->
        <div class="header-icons">

            <div class="header-wrapicon2">
                <li class="fa fa-search fa-2x header-icon1 js-show-header-dropdown"

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <strong>Search:</strong>
                        <div class="header-cart-total">
                        </div>
                        <form method="POST" action="zoeken.php">
                            <input class="zoekbalk" type="text" placeholder="Product" name="zoeken" >
                            <input class="zoekknop" type="submit" value="Zoeken" name="submit">
                        </form>
                    </div>
            </div>

            <span class="linedivide1"></span>

            <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
            </a>

            <span class="linedivide1"></span>

            <div class="header-wrapicon2">
                <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                <span class="header-icons-noti"><?php echo $_SESSION['waardes']; ?></span>
            </div>
        </div>
    </div>
</div>