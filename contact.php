<!DOCTYPE html>
<?php
//Start session
session_start();
// The connection to the database
include 'connect.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<head>
    <title>WWI Contact</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!--===============================================================================================-->
</head>
<div class="header1">
    <!-- Header desktop -->
    <div class="wrap_header">
        <!-- Logo -->
        <a href="index.php" class="logo">
            <img src="images/wwi.png" alt="IMG-LOGO">
        </a>

        <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link dropbtn">Producten</a>
                            <div class="dropdown-content">
                                <?php
                                while ($row = $hresult->fetch_assoc()) {
                                    echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                }
                                ?>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="about.php">Over ons</a>
                    </li>

                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Header Icon -->
        <div class="header-icons">

            <div class="header-wrapicon2">
                <li class="fa fa-search fa-2x header-icon1 js-show-header-dropdown"

                <!-- Header cart noti -->
                <div class="header-cart header-dropdown">
                    <strong>Search:</strong>
                    <div class="header-cart-total">
                    </div>
                    <form method="get" action="zoeken.php">
                        <input class="zoekbalk" type="text" placeholder="Product" name="zoeken" >
                        <input class="zoekknop" type="submit" value="Zoeken" name="submit">
                    </form>
                </div>
            </div>

            <span class="linedivide1"></span>

            <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
            </a>

            <span class="linedivide1"></span>

            <div class="header-wrapicon2">
                <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                <span class="header-icons-noti"><?php echo $_SESSION['waardes'];?></span>
            </div>
        </div>
    </div>
</div>
<body class="animsition">

	<!-- Header -->
	<!-- Header -->


	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
	</section>

	<!-- content page -->
    <?php


    if(isset($_POST['btnSubmit'])) {
        $naam = $_POST['name'];
        $email = $_POST['email'];
        $tel = $_POST['phone-number'];
        $bericht = $_POST['message'];
        $verzonden = "";
        $skey = "6LfrqXwUAAAAAMYG1Dh0G0r1ejsR8pqqlndNzKs6";
        $rkey = $_POST['g-recaptcha-response'];
        $userip = $_SERVER['REMOTE_ADDR'];
        $verzonden = " ";

        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$skey&response=$rkey&remoteip=$userip";
        $response = file_get_contents($url);
        $response = json_decode($response);

        if ($response->success) {

            $to      = 'beijnen.niels@gmail.com';
            $subject = 'Bericht van de website';
            $message = 'Naam:'. " " . $naam. "\r\n";
            $message .= 'Tel:'. " " . $tel. "\r\n";
            $message .= 'Email:'. " " . $email. "\r\n";
            $message .= 'Bericht:'. " " . wordwrap($bericht, 50, "\r\n", true). "\r\n";
            $headers = 'From:'.$email . "\r\n" .
                'Reply-To:'. $email . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);

            $verzonden = TRUE;
        } else {
            $verzonden = FALSE;
        }
    }


    ?>
	<section class="bgwhite p-t-66 p-b-60">
		<div class="container">
			<div class="row">
				<div class="col-md-6 p-b-30">
					<div class="p-r-20 p-r-0-lg">
                                           <div style="width: 100%">
                                              <iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=52.501133,6.079868&amp;q=Campuspad%2C%20P3%2C%208031%20ES%20Zwolle%2C%20Nederland+(World%20Wide%20Importer)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" 
                                                      frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google map generator</a>
                                              </iframe>
                                           </div>
					</div>
				</div>
				<div class="col-md-6 p-b-30">
                    <h3 class="m-text26 p-b-36 p-t-15">
                        Hulp nodig? <br>
                        Stuur ons een bericht.
                    </h3>
					<form class="leave-comment" method="post" action="#">
						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="name" placeholder="Uw Naam">
						</div>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="phone-number" placeholder="Telefoonnummer">
						</div>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email" placeholder="Email Adres">
						</div>

						<textarea class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" name="message" placeholder="Bericht"></textarea>
                        <div class="g-recaptcha" data-sitekey="6LfrqXwUAAAAAFqTWYcw7uRHn2CgnnwU1JIIY4ix"></div>
                            <div>
                                <p><?php
                                    if ($verzonden == FALSE) {
                                        print("Je moet je de reCHAPTA nog accepteren");
                                    } else {
                                        print("Je mail is verzonden!");
                                    }


                                    ?></p>
                            </div>
						<div class="w-size25">
							<!-- Button -->
							<input class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4" name="btnSubmit" type="submit" value="Zenden">
							</input>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>


    <!-- Footer -->
    <footer class="bg6 p-t-45 p-b-43 p-l-65 p-r-65c p-lr-0-xl1">
        <div class="flex-w p-b-90">
            <div class="w-size6 p-t-30 p-l-15 p-r-15 respon6">
                <h4 class="s-text12 p-b-30">
                    Categorieën
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Novelty items
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Clothing
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Mugs
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            T-shirts
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Airline Novelties
                        </a>
                    </li>
                </ul>
            </div>
            <div class="flex-w p-b-90 rijcatagorie">
                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Computing Novelties
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            USB Novelties
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Furry footwear
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Toys
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Packaging materials
                        </a>
                    </li>
                </ul>
            </div>

            <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
                <h4 class="s-text12 p-b-30">
                    Help
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Order traceren
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Retourneren
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            Versturen
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            FAQs
                        </a>
                    </li>
                </ul>
            </div>


        </div>
        <div class="t-center s-text8 p-t-20">
            Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
        </div>
    </footer>
    </div>


    <!-- Back to top -->
    <div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
    </div>

    <!-- Container Selection1 -->
    <div id="dropDownSelect1"></div>



    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/slick/slick.min.js"></script>
    <script type="text/javascript" src="js/slick-custom.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>
</body>
</html>