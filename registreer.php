<html>
    <?php
//Start session
    session_start();
// The connection to the database
    include 'connect.php';
    include 'functions.php';
    $hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s
            JOIN StockItemStockGroups g
            ON s.stockitemid = g.stockitemid
            JOIN StockGroups sg
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
    $hresult = $conn->query($hsql);
    if (!$hresult->num_rows > 0) {
        echo "0 results";
        return;
    }
    $conn->close();
    ?>
    <head>
        <title>WWI</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
        <link rel="stylesheet" type="text/css" href="css/inlog.css">
        <link rel="stylesheet" type="text/css" href="css/register.css">
        <!--===============================================================================================-->
    </head>
    <div class="header1">
        <!-- Header desktop -->
        <div class="wrap_header">
            <!-- Logo -->
            <a href="index.php" class="logo">
                <img src="images/wwi.png" alt="IMG-LOGO">
            </a>

            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <?php
                                    while ($row = $hresult->fetch_assoc()) {
                                        echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="about.php">Over ons</a>
                        </li>

                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <!-- Header Icon -->
            <div class="header-icons">

                <div class="header-wrapicon2">
                    <li class="fa fa-search fa-2x header-icon1 js-show-header-dropdown"

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <strong>Search:</strong>
                            <div class="header-cart-total">
                            </div>
                            <form method="get" action="zoeken.php">
                                <input class="zoekbalk" type="text" placeholder="Product" name="zoeken" >
                                <input class="zoekknop" type="submit" value="Zoeken" name="submit">
                            </form>
                        </div>
                </div>

                <span class="linedivide1"></span>

                <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                    <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                    <span class="header-icons-noti"><?php echo $_SESSION['waardes']; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container1-page">
        <!-- Slide1 -->

        <!-- Title Page -->
        <section class="bg-title-page1 p-t-50 p-b-900 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
            <h2 class="l-text2 t-center">
                <br>
            </h2>
            <p class="m-text13 t-center">
                <br>
            </p>
        </section>
        <body>
            <div multilinks-noscroll="true" id="content" role="main" class="clearfix"/>


            <h1> Registratie </h1>

            <form class="registratie" action="registreer.php" method="POST">
                <table>
                    <tr>
                        <td>Voornaam:</td> <td><input type="text" name="voornaam" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['voornaam'];
                            }
                            ?>" placeholder="Henk"/></td>
                    </tr>
                    <tr>
                        <td>Tussenvoegsel:</td> <td><input type="text" name="tussenvoegsel" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['tussenvoegsel'];
                            }
                            ?>" placeholder="de"/></td>
                    </tr>
                    <tr>
                        <td>Achternaam:</td> <td><input type="text" name="achternaam" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['achternaam'];
                            }
                            ?>" placeholder="Vries"/></td>
                    </tr>
                    <tr>
                        <td>*Adres:</td> <td><input type="text" name="adres" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['adres'];
                            }
                            ?>" placeholder="Langeweg 123"/></td>
                    </tr>
                    <tr>
                        <td>*Postcode:</td> <td><input type="text" name="postcode" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['postcode'];
                            }
                            ?>" placeholder="1234AA"/></td>
                    </tr>
                    <tr>
                        <td>*Woonplaats:</td> <td><input type="text" name="woonplaats" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['woonplaats'];
                            }
                            ?>" placeholder="Amsterdam"/></td>
                    </tr>
                    <tr>
                        <td>*Telefoonnummer:</td> <td><input type="text" name="telefoonnummer" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['telefoonnummer'];
                            }
                            ?>" placeholder="06-12345678"/></td>
                    </tr>
                    <tr>
                        <td>*Email adres:</td> <td><input type="text" name="LogonName" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['LogonName'];
                            }
                            ?>" placeholder="feest@worldwideimporters.com"/></td>
                    </tr>
                    <tr>
                        <td>*Wachtwoord:</td> <td><input type="password" name="wachtwoord1"/></td>
                    </tr>
                    <tr>
                        <td>*Wachtwoord herhalen:</td> <td><input type="password" name="wachtwoord2"/></td>
                    </tr>
                </table>
                <div  style="text-align:center;">
                    <input type="submit" name="registreer" value="Aanmelden"/>
                </div>
            </form>

            <h4> Velden met * zijn verplicht </h4>

        </body>
</html>

<?php
//connectie PDO
$user = "root";
$pass = "";
$dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);

//variabelen aanmaken
if (isset($_POST["registreer"])) {
    $voornaam = $_POST["voornaam"];
    $tussenvoegsel = $_POST["tussenvoegsel"];
    $achternaam = $_POST["achternaam"];
    $adres = $_POST["adres"];
    $postcode = $_POST["postcode"];
    $woonplaats = $_POST["woonplaats"];
    $telefoonnummer = $_POST["telefoonnummer"];
    $LogonName = $_POST["LogonName"];
    $wachtwoord1 = $_POST["wachtwoord1"];
    $wachtwoord2 = $_POST["wachtwoord2"];
    $naam = naamplakken($voornaam, $tussenvoegsel, $achternaam);
}

// controle of alle verplichte velden zijn ingevuld
if (isset($_POST["registreer"])) {
    if (controleervelden($naam, $telefoonnummer, $adres, $postcode, $woonplaats, $LogonName, $wachtwoord1, $wachtwoord2) AND controleermail($LogonName) == TRUE) {
        $wachtwoord3 = hash('sha256', $wachtwoord1);

        //query met alle ingevulde gegevens
        $sth = $dbh->prepare("INSERT INTO klant (naam, telefoonnummer, adres, postcode, woonplaats, LogonName, HashedPassword, IsPermittedToLogon)
            VALUES (:naam, :telefoonnummer, :adres, :postcode, :woonplaats, :LogonName, :wachtwoord3, 1)");

        //beveiliging
        $sth->bindValue(':naam', $naam, PDO::PARAM_STR);
        $sth->bindValue(':telefoonnummer', $telefoonnummer, PDO::PARAM_STR);
        $sth->bindValue(':adres', $adres, PDO::PARAM_STR);
        $sth->bindValue(':postcode', $postcode, PDO::PARAM_STR);
        $sth->bindValue(':woonplaats', $woonplaats, PDO::PARAM_STR);
        $sth->bindValue(':LogonName', $LogonName, PDO::PARAM_STR);
        $sth->bindValue(':wachtwoord3', $wachtwoord3, PDO::PARAM_STR);

        $sth->execute();
        $aantal = $sth->rowCount();

        if ($aantal == 1) {
            echo "<br>Registratie gelukt u kunt nu inloggen.";
        } else {
            echo "<br><br>Er is een fout op getreden neemt u a.u.b contact op met de beheerder van deze website.";
        }
    }
}
?>

