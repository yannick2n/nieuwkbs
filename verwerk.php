<?php

session_start();
$id = htmlspecialchars($_GET["id"]);
$action = htmlspecialchars($_GET["action"]);

if ($action == "add") {
    function verhoog($id) {
        foreach ($_SESSION as $i => $w) {
            if ($i == 'id' . $id) {
                //verhoog waarde met 1
                $_SESSION['id' . $id] = $w + 1;
                return true;
            } 
        }
        return false;
    }

    if (!verhoog($id)) {
        $_SESSION['id' . $id] = '1';
    }
} else if ($action == "remove") {
    foreach ($_SESSION as $i => $w) {
        echo $w;
        if ($w != 1) {
            $_SESSION['id' . $id] = $w - 1;
        } else {
            unset($_SESSION['id' . $id]);
        }
    }
}

if ($action == "removeall") {
    unset($_SESSION['id' . $id]);
}


header("Location: Winkelwagen.php");


