<?php
//Start session
session_start();
// The connection to the database
include 'connect.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<head>
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    <!--===============================================================================================-->
</head>
<div class="header1">
    <!-- Header desktop -->
    <div class="wrap_header">
        <!-- Logo -->
        <a href="index.php" class="logo">
            <img src="images/wwi.png" alt="IMG-LOGO">
        </a>

        <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link dropbtn">Producten</a>
                            <div class="dropdown-content">
                                <?php
                                while ($row = $hresult->fetch_assoc()) {
                                    echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                }
                                ?>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="about.php">Over ons</a>
                    </li>

                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Header Icon -->
        <div class="header-icons">

            <div class="header-wrapicon2">
                <li class="fa fa-search fa-2x header-icon1 js-show-header-dropdown"

                <!-- Header cart noti -->
                <div class="header-cart header-dropdown">
                    <strong>Search:</strong>
                    <div class="header-cart-total">
                    </div>
                    <form method="post" action="zoeken.php">
                        <input class="zoekbalk" type="text" placeholder="Product" name="zoeken" >
                        <input class="zoekknop" type="submit" value="Zoeken" name="submit">
                    </form>
                </div>
            </div>

            <span class="linedivide1"></span>

            <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
            </a>

            <span class="linedivide1"></span>

            <div class="header-wrapicon2">
                <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                <span class="header-icons-noti"><?php echo $_SESSION['waardes'];?></span>
            </div>
        </div>
    </div>
</div>
<body>
    
<!-- Header -->



<!-- Title Page -->
<section class="bg-title-page p-t-50a p-b-900 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
    <h2 class="l-text2 t-center">
        <br>
    </h2>
    <p class="m-text13 t-center">
        <br>
    </p>
</section>

<?php
// The connection to the database
include 'connect.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<!-- Content page -->
<section class="bgwhite p-t-55 p-b-65">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
                <div class="leftbar p-r-20 p-r-0-sm">
                    <!--  -->
                    <h4 class="m-text14 p-b-7">
                        Categorieen
                    </h4>

                    <ul class="p-b-54">
                        <?php
                        while ($row = $hresult->fetch_assoc()) {
                            echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                        }
                        ?>
                    </ul>

                    <div class="search-product pos-relative bo4 of-hidden">
                        <input class="s-text7 size6 p-l-23 p-r-50" type="text" name="search-product" placeholder="Search Products...">

                        <button class="flex-c-m size5 ab-r-m color2 color0-hov trans-0-4">
                            <i class="fs-12 fa fa-search" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
                <!--  -->
                    <?php
                    /// Database connection
                    include 'connect.php';
                    $id = htmlspecialchars($_GET["id"]);
                    $orderby = "";
                    $max = " LIMIT 7 ";

                    if (isset($_GET["ob"])) {
                        $ob = htmlspecialchars($_GET["ob"]);
                        if ($ob == "name") {
                            $orderby = "ORDER BY StockitemName ";
                        } elseif ($ob == "priceup") {
                            $orderby = "ORDER BY recommendedretailprice ";
                        } elseif ($ob == "pricedown") {
                            $orderby = "ORDER BY recommendedretailprice desc ";
                        }
                    }

                    if (isset($_GET["max"])) {
                        if (htmlspecialchars($_GET["max"]) != 0) {
                            $max = " LIMIT " . (htmlspecialchars($_GET["max"]) + 1);
                        } else {
                            $max = "";
                        }
                    }

                    /// SQL query to get item
                    $sql = "SELECT s.stockitemid, s.Photo, StockitemName, recommendedretailprice, sg.stockgroupname, sg.stockgroupid
                FROM StockItems s 
                JOIN StockItemStockGroups g 
                ON s.stockitemid = g.stockitemid
                JOIN StockGroups sg 
                ON g.stockgroupid = sg.StockGroupID
                WHERE sg.stockgroupid = $id $orderby $max";
                    $result = $conn->query($sql);
                    if (!$result->num_rows > 0) {
                        echo "0 results";
                        return;
                    }

                    /// Select item based on selected categorie
                    $sql2 = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
                    $result2 = $conn->query($sql2);
                    if (!$result2->num_rows > 0) {
                        echo "0 results";
                        return;
                    }

                    /// Closses the database connection
                    $conn->close();

                    /// Return all parameters in a array
                    function returnGetParams() {
                        $array = array();
                        $array["id"] = htmlspecialchars($_GET["id"]);
                        if (isset($_GET["ob"])) {
                            $array["ob"] = htmlspecialchars($_GET["ob"]);
                        } if (isset($_GET["max"])) {
                            $array["max"] = htmlspecialchars($_GET["max"]);
                        }
                        return $array;
                    }
                    ?>
        <?php
        $i = 1;

        /// Gets all info from HTLM about products
        while ($row = $result->fetch_assoc()) {
            $name = $row["StockitemName"];
            $price = $row["recommendedretailprice"];
            $category = $row["stockgroupname"];
            $iid = $row["stockitemid"];
            $id = $row["stockgroupid"];
            $max = 10;
            if (isset($_GET["max"])) {
                $max = htmlspecialchars($_GET["max"]) + 1;
            }
            if ($i != 0) {
                echo "<b>$category:</b><br><br>";
                echo 'Sorteer op: <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                echo 'ob=name">naam</a>, <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                echo 'ob=priceup">prijs oplopend</a>, <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                ?>
                <?php
                echo 'ob=pricedown">prijs aflopend</a>.<br>';
                echo 'Maximaal per pagina: <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                echo 'max=9">9</a> <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                echo 'max=12">12</a> <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                echo 'max=24">24</a> <a href="categorie.php?';
                foreach (returnGetParams() as $index => $value) {
                    echo "$index=$value&";
                }
                echo 'max=0">all</a>';
                echo '<div class="row"><table>';
                $i--;
            } else {
                
                            echo  '<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">'.
                    ' <div class="block2">'.
                    ' <div class="block2-img wrap-pic-w of-hidden pos-relative">'.
                        
                    " <img src='data:image/jpeg;base64," . base64_encode($row['Photo']) . "'>".

                    ' <div class="block2-overlay trans-0-4">'.
                    ' <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">'.
                    ' <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>'.
                    '   <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>'.
                    '   </a>'.

                    ' <div class="block2-btn-addcart w-size1 trans-0-4">'.
                    '<form method="post" action="productpagina.php?action=add&id=' . $iid . '">'.
                    '  <a class="btnAddAction" href="verwerk.php?id=' . $iid . '&action=add"><p><button class="buttonwinkel">&nbsp;&nbsp;Winkelwagen&nbsp;&nbsp;</button></p></a></div>'.
                    ' </form>'.
                    ' </div>'.
                    '  </div>'.
                    ' </div>'.

                    ' <div class="block2-txt p-t-20">'.
                    ' <a href="productpagina.php?action=add&id=' . $iid . '" class="block2-name dis-block s-text3 p-b-5">'.
                     $name .
                    ' </a>'.

                    '<span class="block2-price m-text6 p-r-5">'.
                     $price .
                    '</span>'.
                    ' </div>'.
                    ' </div>'.
                    '  </div>';
                      
              
                        
                ?>
                <a href="verwerk.php?id=<?php echo $iid; ?>&action=add"></a></div>
            <?php
        }
    }
    echo '</div></table></div>';
    ?>


<!-- Footer -->
<footer class="bg6 p-t-45 p-b-43 p-l-65 p-r-65b p-lr-0-xl1">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon6">
            <h4 class="s-text12 p-b-30">
                Categorieën
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Novelty items
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Clothing
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Mugs
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        T-shirts
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Airline Novelties
                    </a>
                </li>
            </ul>
        </div>
        <div class="flex-w p-b-90 rijcatagorie">
            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Computing Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        USB Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Furry footwear
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Toys
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Packaging materials
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
            <h4 class="s-text12 p-b-30">
                Help
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Order traceren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Retourneren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Versturen
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        FAQs
                    </a>
                </li>
            </ul>
        </div>


    </div>
    <div class="t-center s-text8 p-t-20">
        Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
    </div>
</footer>
</div>


<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
</div>

<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $('.block2-btn-addcart').each(function(){
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function(){
            swal(nameProduct, "is added to cart !", "success");
        });
    });

    $('.block2-btn-addwishlist').each(function(){
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function(){
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
</script>

<!--===============================================================================================-->
<script src="js/main.js"></script>
</body>
</html>