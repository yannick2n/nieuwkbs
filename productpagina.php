<!DOCTYPE html>
<?php
include 'header.php';
include 'db.php';
?>
<html>
    <head>
        <?php
        include 'db.php';
        $post = htmlspecialchars($_GET["id"]);

        $sql1 = "SELECT StockitemName, s.Photo, recommendedretailprice, sg.stockgroupname, marketingcomments
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            WHERE s.StockitemID = " . $post;
        $result1 = $conn->query($sql1);
        if ($result1->num_rows > 0) {
            $category = array();
            while ($row = $result1->fetch_assoc()) {
                $name = $row["StockitemName"];
                $price = $row["recommendedretailprice"];
                $category[] = $row["stockgroupname"];
                $comment = $row["marketingcomments"];
                $photo = $row["Photo"];
            }
        } else {
            echo "1: 0 results";
        }
        $conn->close();
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!--===============================================================================================-->
        <meta charset="UTF-8">
        <title>WWI | <?php echo $name; ?></title>
        <link rel="stylesheet" type="text/css" href="css/styleproduct.css">
    </head>
   
    <body>

        <!-- Begin van header gedeelte -->

        <!-- Einde van header gedeelte -->

        <br><br>
        <div class="wrapper">
            <div class="product_left">
                 <?php
                echo " <img src='data:image/jpeg;base64," . base64_encode($photo) . "'>";
                ?>
            </div>
            <div class="product_right">
                <h2><?php echo $name; ?></h2>
                <h3><b>€<?php echo $price; ?> </b><i> Excl. Verzendkosten</i></h3>
                <p><?php echo $comment ?></p>
                <a href="verwerk.php?id=<?php echo $post; ?>&action=add"><input class="buttonclass" type="submit" value="In winkelwagen" name="addtocart" /></a><br><br>
                <b>Categorieën: </b> 
                <?php
                $i = sizeof($category);
                foreach ($category as $cat) {
                    $i--;
                    if ($i != 0) {
                        echo "$cat, ";
                    } else {
                        echo $cat;
                    }
                }
                ?>
            </div>
        </div>

    </body>
</html>
