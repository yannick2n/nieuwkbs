<html>
<?php
//Start session
session_start();
// The connection to the database
include 'connect.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<head>
    <title>WWI</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    <link rel="stylesheet" type="text/css" href="css/inlog.css">
    <!--===============================================================================================-->
</head>
<div class="header1">
    <!-- Header desktop -->
    <div class="wrap_header">
        <!-- Logo -->
        <a href="index.php" class="logo">
            <img src="images/wwi.png" alt="IMG-LOGO">
        </a>

        <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link dropbtn">Producten</a>
                            <div class="dropdown-content">
                                <?php
                                while ($row = $hresult->fetch_assoc()) {
                                    echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                }
                                ?>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="about.php">Over ons</a>
                    </li>

                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Header Icon -->
        <div class="header-icons">

            <div class="header-wrapicon2">
                <li class="fa fa-search fa-2x header-icon1 js-show-header-dropdown"

                <!-- Header cart noti -->
                <div class="header-cart header-dropdown">
                    <h3><strong>Zoeken:</strong></h3>
                    <div class="header-cart-total">
                    </div>
                    <form method="get" action="zoeken.php">
                        <div class="balk">
                        <input class="zoekbalk" type="text" placeholder="Product" name="zoeken" >
                        </div>
                        <div class="knop">
                        <input class="zoekknop" type="submit" value="&nbsp;Zoeken&nbsp;" name="submit">
                        </div>
                    </form>
                </div>
            </div>

            <span class="linedivide1"></span>

            <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
            </a>

            <span class="linedivide1"></span>

            <div class="header-wrapicon2">
                <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                <span class="header-icons-noti"><?php echo $_SESSION['waardes'];?></span>
            </div>
        </div>
    </div>
</div>
    <body class="animsition">


    <body>
    <div class="borderinlog">
        <article multilinks-noscroll="true" id="content" class="clearfix">
            <div class="page-header" align='center'>
                <h1 class="textinlog"><b>Inlogpagina</b></h1>
            </div>
            <form class="form-signin-signup" action="controleerwachtwoord.php" method="post" >
                <p> Inlognaam: </p> <input class='inlogscherm' type="text" name="LogonName" placeholder="E-mail" size="35">
                <br>
                <p> Wachtwoord: </p><input class='inlogscherm' type="password" name="wachtwoord" placeholder="Wachtwoord" size="35">

                <div class="tekst-login row">
                    <div class="wachtwoordvergeten">
                    <a class="wwvergeten" href="contact.php">&nbsp;&nbsp;Wachtwoord  vergeten?&nbsp;&nbsp;</a>
                    </div>
                    &nbsp;&nbsp;
                    <div class="registeren">
                    <a class="registercolor" href="registreer.php">&nbsp;&nbsp;Registreren&nbsp;&nbsp;</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="ruimte">
                <input type="submit" value="Inloggen" class="btn btn-primary btn-large">
                </div>
            </form>
        </article>
    </div>
    </body>


<!-- Footer -->

<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90 row">
        <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4 col-sm-4">

            <h4 class="s-text12 p-b-30">
                Categorieën
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Novelty items
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Clothing
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Mugs
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        T-shirts
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Airline Novelties
                    </a>
                </li>
            </ul>
        </div>
        <div class="flex-w p-b-90 rijcatagorie">
            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Computing Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        USB Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Furry footwear
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Toys
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Packaging materials
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
            <h4 class="s-text12 p-b-30">
                Help
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Order traceren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Retourneren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Versturen
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        FAQs
                    </a>
                </li>
            </ul>
        </div>


    </div>
    <div class="t-center s-text8 p-t-20">
        Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
    </div>
</footer>
<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
        <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
</div>

<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick-custom.js"></script>
<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $('.block2-btn-addcart').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
</script>

<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>