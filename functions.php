<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function controleermail($email) {
    $user = "root";
    $pass = "";
    $dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);

    $sth = $dbh->prepare("SELECT * FROM klant WHERE LogonName = :LogonName");
    $sth->bindValue(':LogonName', $email, PDO::PARAM_STR);

    $sth->execute();

    $sth->execute();
    $aantal = $sth->rowCount();

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "<br><br>Onjuist email adres ingevuld";
        return FALSE;
    } elseif ($aantal != 0) {
        echo "<br><br>Er bestaat al een account met het opgegeven e-mailadres.";
        return FALSE;
    } else {
        return TRUE;
    }
}

function controleervelden($naam, $telefoonnummer, $adres, $postcode, $woonplaats, $email, $wachtwoord1, $wachtwoord2) {
    if (preg_match("#[a-z]+#", $telefoonnummer) OR strlen($telefoonnummer) > 20 OR strlen($telefoonnummer) < 8) {
        echo "<br><br>Ongeldig telefoonnummer ingevuld";
        return FALSE;
    } elseif (preg_match("#[0-9]+#", $naam) OR strlen($naam) > 50 OR preg_match('/[^a-z\s-]/i', $naam)) {
        echo "<br><br> Ongeldige naam ingevuld";
        return FALSE;
    } elseif (strlen($adres) > 30 OR strlen($adres) < 8 OR ! preg_match('/[^a-z\s-]/i', $adres)) {
        echo "<br><br>Ongeldig adres ingevuld";
        return FALSE;
    } elseif (strlen($postcode) < 5 OR strlen($postcode) > 8 OR ! preg_match('/[^a-z\s-]/i', $postcode)) {
        echo "<br><br>Ongeldige postcode ingevuld";
        return FALSE;
    } elseif (empty($woonplaats) OR strlen($postcode) > 30) {
        return FALSE;
        echo "<br><br>Woonplaats niet ingevuld";
    } elseif (empty($email)) {
        return FALSE;
        echo "<br><br>Email niet ingevuld";
    } elseif ($wachtwoord1 != $wachtwoord2 OR empty($wachtwoord1)) {
        echo "<br><br>Ingevulde wachtwoorden komen niet overeen";
        return FALSE;
    } elseif (strlen($wachtwoord1) < 6 OR strlen($wachtwoord1) > 20 OR ! preg_match("#[0-9]+#", $wachtwoord1) OR ! preg_match("#[a-z]+#", $wachtwoord1)) {
        echo"<br><br>Wachtwoord voldoet niet aan de gestelde eisen: minimaal 6 tekens waarvan minimaal 1 cijfer.";
        return FALSE;
    } else {
        return TRUE;
    }
}

function naamplakken($voornaam, $tussenvoegsel, $achternaam) {
    return "$voornaam $tussenvoegsel $achternaam";
}

?>